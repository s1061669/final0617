﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Final_0617.Models
{
    [MetadataType(typeof(NewsMetaData))]
    public partial class News
    {
    }

    public class NewsMetaData
    {
        [Required(ErrorMessage = "必填")]
        [DisplayName("標題")]
        [StringLength(50)]
        public string Title { get; set; }

        [Required(ErrorMessage = "必填")]
        [DisplayName("內容")]
        [StringLength(500)]
        public string Content { get; set; }

        [Required(ErrorMessage = "必填")]
        [DisplayName("發布日期")]
        public System.DateTime PublishData { get; set; }
    }

}