﻿using Final_0617.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final_0617.Controllers
{
    public class WeekController : Controller
    {
        // GET: Week
        public ActionResult Index()
        {
            return View(new WeekData());
        }

        [HttpPost]
        public ActionResult Index(WeekData data)
        {
            float Week = data.Week;
            string ch_week = "";
            if (Week == 1)
            {
                ch_week = "星期一";
            }
            else if (Week == 2)
            {
                ch_week = "星期二";
            }
            else if (Week == 3)
            {
                ch_week = "星期三";
            }
            else if (Week == 4)
            {
                ch_week = "星期四";
            }
            else if (Week == 5)
            {
                ch_week = "星期五";
            }
            else if (Week == 6)
            {
                ch_week = "星期六";
            }
            else if (Week == 7)
            {
                ch_week = "星期日";
            }
            else {
                ch_week = "輸入錯誤";
            }

            data.Ch_week = ch_week;
            return View(data);
        }
    }
}